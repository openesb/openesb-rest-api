package net.openesb.rest.api.resources;

import javax.ws.rs.core.Application;
import net.openesb.management.api.ServiceAssemblyService;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.mockito.Mockito;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public abstract class AbstractResourceTest extends JerseyTest {
    
    protected static ServiceAssemblyService serviceAssemblyServiceMock = Mockito.mock(ServiceAssemblyService.class);
    
    @Override
    protected Application configure() {
        ResourceConfig rc = new ResourceConfig();
        
        rc.packages("net.openesb.rest.api.resources", "net.openesb.rest.api.provider");
        rc.register(new MockServiceInjectionProvider());
        rc.register(MultiPartFeature.class);
        rc.register(JacksonFeature.class);
        
        return rc;
    }

    @Override
    protected void configureClient(ClientConfig clientConfig) {
        super.configureClient(clientConfig);
        
        clientConfig.register(JacksonFeature.class);
        clientConfig.register(MultiPartFeature.class);
    }
    
    class MockServiceInjectionProvider extends AbstractBinder {

        @Override
        protected void configure() {
            bind(serviceAssemblyServiceMock).to(ServiceAssemblyService.class);
        }
        
    }
}
