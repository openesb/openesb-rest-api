package net.openesb.rest.utils;

import com.sun.jbi.management.descriptor.Consumer;
import com.sun.jbi.management.descriptor.Consumes;
import com.sun.jbi.management.descriptor.Provider;
import com.sun.jbi.management.descriptor.Provides;
import net.openesb.rest.api.model.ui.resolver.ConnectionPoint;

public final class ConnectionPointBuilder {

    private ConnectionPointBuilder() {
    }

    public static ConnectionPoint getConnectionPoint(Provider provider) {
        return new ConnectionPoint(provider.getServiceName(), provider.getEndpointName());
    }

    public static ConnectionPoint getConnectionPoint(Consumer consumer) {
        return new ConnectionPoint(consumer.getInterfaceName(), consumer.getServiceName(), consumer.getEndpointName());
    }

    public static ConnectionPoint getConnectionPoint(Provides provides) {
        return new ConnectionPoint(provides.getInterfaceName(), provides.getServiceName(), provides.getEndpointName());
    }

    public static ConnectionPoint getConnectionPoint(Consumes consumes) {
        return new ConnectionPoint(consumes.getInterfaceName(), consumes.getServiceName(), consumes.getEndpointName());
    }
}
