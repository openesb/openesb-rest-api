package net.openesb.rest.api.resources;

import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.container.ResourceContext;
import net.openesb.rest.api.annotation.RequiresAuthentication;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@RequiresAuthentication
public class ComponentApplicationResource {
    
    @Inject
    private ResourceContext resourceContext;
    private final String componentName;

    public ComponentApplicationResource(String componentName) {
        this.componentName = componentName;
    }
    
    @Path("variables")
    public ComponentApplicationVariableResource getComponentApplicationVariableResource() {
        return resourceContext.initResource(
                new ComponentApplicationVariableResource(componentName));
    }
    
    @Path("configurations")
    public ComponentApplicationConfigurationResource getComponentApplicationConfigurationResource() {
        return resourceContext.initResource(
                new ComponentApplicationConfigurationResource(componentName));
    }
}
