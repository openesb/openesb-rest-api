package net.openesb.rest.api.resources;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class LifecycleActionParam {
    
    public enum LifecycleAction {
        START,
        STOP,
        SHUTDOWN
    }
    
    private LifecycleAction action ;
    
    public LifecycleActionParam(String input) {
        try {
            if (input != null) {
                action = LifecycleAction.valueOf(input.toUpperCase());
            }
        } catch (IllegalArgumentException iae) {
            // Nothing to do here
        }
    }
    
    public LifecycleAction getAction() {
        return this.action;
    }
}
