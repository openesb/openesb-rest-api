package net.openesb.rest.api.resources;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import net.openesb.model.api.Logger;
import net.openesb.management.api.AdministrationService;
import net.openesb.management.api.ManagementException;
import net.openesb.rest.api.annotation.RequiresAuthentication;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@RequiresAuthentication
public class InstanceLoggersResource extends AbstractResource {
    
    @Inject
    private AdministrationService administrationService;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Logger> getLoggers() throws ManagementException {
        Set<Logger> loggers = new TreeSet<Logger>(new Comparator <Logger>() {
                @Override
                public int compare(Logger log1, Logger log2) {
                    return log1.getName().compareTo(log2.getName());
                }
            });
        
        loggers.addAll(administrationService.getLoggers());
        
        return loggers;
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Logger> setLevel(@QueryParam("logger") String loggerName, @QueryParam("level") LoggerLevelParam level) throws ManagementException {
        administrationService.setLoggerLevel(loggerName, level.getLevel());
        
        // Return the list of level
        return getLoggers();
    }
}
