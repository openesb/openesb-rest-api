package net.openesb.rest.api.model.ui.resolver;

import com.sun.jbi.management.descriptor.Connection;
import com.sun.jbi.management.descriptor.Jbi;
import com.sun.jbi.management.descriptor.Provider;
import com.sun.jbi.management.descriptor.Provides;
import com.sun.jbi.management.descriptor.Services;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Loic DASSONVILLE (ldassonville.openesb at gmail.com)
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ProvidesResolver extends Resolver<Provides> {

    public ProvidesResolver(Jbi jbiServiceUnit, String targetComponent) {
        super(jbiServiceUnit, targetComponent);
    }

    @Override
    protected ConnectionPoint toConnectionPoint(Connection connection) {
        Provider provider = connection.getProvider();
        return new ConnectionPoint(provider.getServiceName(), provider.getEndpointName());
    }

    @Override
    protected ConnectionPoint toConnectionPoint(Provides entity) {
        return new ConnectionPoint(entity.getServiceName(), entity.getEndpointName());
    }

    @Override
    protected List<Provides> extractEntities(Services services) {
        if (services == null) {
            return new ArrayList<Provides>();
        }
        return services.getProvides();
    }
}
