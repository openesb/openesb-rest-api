package net.openesb.rest.api.service.ui;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import net.openesb.management.api.ManagementException;
import net.openesb.management.api.ManagementFactory;
import net.openesb.management.api.ServiceAssemblyService;

import com.sun.jbi.management.descriptor.Connection;
import com.sun.jbi.management.descriptor.Consumer;
import com.sun.jbi.management.descriptor.Jbi;
import com.sun.jbi.management.descriptor.Provider;
import com.sun.jbi.management.descriptor.ServiceUnit;
import net.openesb.rest.api.model.ui.Connector;
import net.openesb.rest.api.model.ui.Consumes;
import net.openesb.rest.api.model.ui.Model;
import net.openesb.rest.api.model.ui.Provides;
import net.openesb.rest.api.model.ui.resolver.ConsumesResolver;
import net.openesb.rest.api.model.ui.resolver.Link;
import net.openesb.rest.api.model.ui.resolver.ProvidesResolver;
import net.openesb.rest.utils.ConnectionPointBuilder;
import net.openesb.rest.utils.Mapper;
import net.openesb.rest.utils.UIDUtils;

/**
 *
 * @author Loïc DASSONVILLE (ldassonville.openesb at gmail.com)
 * @author OpenESB Community
 */
public class MapServiceImpl implements MapService {

    private static final String ENGINE_SUFFIX = "-engine";
    private ServiceAssemblyService serviceAssemblyService = ManagementFactory.getServiceAssemblyService();

    private JAXBContext mJbiDescriptorContext;

    private static final Logger LOG = Logger.getLogger(MapServiceImpl.class.getPackage().getName());

    /**
     * Load JBI jaxb context
     */
    protected synchronized JAXBContext getJbiDescriptorContext()
            throws Exception {
        if (mJbiDescriptorContext == null) {
            ClassLoader cl = Class.forName(
                    "com.sun.jbi.management.descriptor.Jbi").getClassLoader();
            mJbiDescriptorContext = JAXBContext.newInstance(
                    "com.sun.jbi.management.descriptor", cl);
        }

        return mJbiDescriptorContext;
    }

    @Override
    public Model getMapModel(String assemblyName) throws ManagementException {

        LOG.info("getMapModel " + assemblyName);
        Model model = new Model();

        try {
            Unmarshaller unmarshaller = getJbiDescriptorContext().createUnmarshaller();
            String descriptorContent = serviceAssemblyService.getDescriptorAsXml(assemblyName, null);
            StringReader reader = new StringReader(descriptorContent);

            Jbi jbi = (Jbi) unmarshaller.unmarshal(reader);

            com.sun.jbi.management.descriptor.ServiceAssembly serviceAssembly = jbi
                    .getServiceAssembly();
            List<ServiceUnit> serviceUnits = serviceAssembly.getServiceUnit();

            List<ConsumesResolver> consumesResolvers = new ArrayList<ConsumesResolver>();
            List<ProvidesResolver> providesResolvers = new ArrayList<ProvidesResolver>();

            if (serviceUnits != null) {
                for (ServiceUnit serviceUnit : serviceUnits) {
                    String suName = serviceUnit.getIdentification().getName();

                    reader = new StringReader(serviceAssemblyService.getDescriptorAsXml(assemblyName,
                            suName));
                    Jbi serviceUnitJbi = (Jbi) unmarshaller.unmarshal(reader);

                    String target = serviceUnit.getTarget().getComponentName();
                    consumesResolvers.add(new ConsumesResolver(serviceUnitJbi,
                            target));
                    providesResolvers.add(new ProvidesResolver(serviceUnitJbi,
                            target));

                    if (!serviceUnitJbi.getServices().isBindingComponent()) {
                        model.getAssemblies().add(
                                Mapper.getCasa(suName, serviceUnitJbi));
                    }
                }
            }

            Map<String, Connector> connectorsMap = new HashMap<String, Connector>();
            for (Connection connection : serviceAssembly.getConnections()
                    .getConnection()) {
                int nbResolution = 0;

                Consumer consumer = connection.getConsumer();
                Provider provider = connection.getProvider();

                Link link = new Link(
                        ConnectionPointBuilder.getConnectionPoint(consumer),
                        ConnectionPointBuilder.getConnectionPoint(provider));

                for (ProvidesResolver providesResolver : providesResolvers) {
                    Provides provides = new Provides(
                            providesResolver.resolve(connection));
                    if (provides.getTarget() != null) {
                        nbResolution++;

                        if (providesResolver.getTargetComponent().endsWith(ENGINE_SUFFIX)) {
                            LOG.log(
                                    Level.FINE,
                                    "Ignoring provides : [{}] [{}] [{}]",
                                    new Object[]{
                                        provides.getDisplayName(),
                                        provides.getProcessName(),
                                        provides.getTarget()
                                        .getInterfaceName()});
                            continue;
                        }
                        link.setType(providesResolver.getTargetComponent());

                        Connector connector = new Connector();
                        connector
                                .setType(providesResolver.getTargetComponent());
                        connector.setName(provides.getTarget()
                                .getInterfaceName().getLocalPart());
                        connector.setConnectionPoint(ConnectionPointBuilder
                                .getConnectionPoint(provides.getTarget()));
                        connectorsMap.put(
                                UIDUtils.getUID(provides.getTarget()),
                                connector);

                        LOG.log(
                                Level.FINE,
                                "[P][{}]Provides resolved {} ({})",
                                new Object[]{
                                    nbResolution,
                                    provides.getTarget().getEndpointName(),
                                    connection.getProvider()
                                    .getServiceName()});
                    }
                }

                // Probably an internal module
                if (nbResolution == 0) {
                    LOG.log(Level.FINE, "Unresolved provide : {}",
                            connection.getProvider().getEndpointName());

                    Connector connector = new Connector();
                    connector.setType("sun-bpel-engine");
                    connector.setName(provider.getEndpointName());
                    connector.setConnectionPoint(ConnectionPointBuilder
                            .getConnectionPoint(provider));
                    connectorsMap.put(UIDUtils.getUID(provider), connector);
                }

                nbResolution = 0;
                for (ConsumesResolver consumeResolver : consumesResolvers) {
                    Consumes consumes = new Consumes(
                            consumeResolver.resolve(connection));
                    if (consumes.getTarget() != null) {
                        nbResolution++;
                        if (consumeResolver.getTargetComponent().endsWith(ENGINE_SUFFIX)) {
                            LOG.log(
                                    Level.FINE,
                                    "Ignoring consumes : [{}] [{}] [{}]",
                                    new Object[]{
                                        consumes.getDisplayName(),
                                        consumes.getProcessName(),
                                        consumes.getTarget()
                                        .getInterfaceName()});
                            continue;
                        }
                        link.setType(consumeResolver.getTargetComponent());

                        Connector connector = new Connector();
                        connector.setType(consumeResolver.getTargetComponent());
                        connector.setName(consumes.getTarget()
                                .getInterfaceName().getLocalPart());
                        connector.setConnectionPoint(ConnectionPointBuilder
                                .getConnectionPoint(consumes.getTarget()));
                        connectorsMap.put(
                                UIDUtils.getUID(consumes.getTarget()),
                                connector);

                        LOG.log(
                                Level.FINE,
                                "[C][{}]Consumer resolved {} ({})",
                                new Object[]{
                                    nbResolution,
                                    consumes.getTarget().getEndpointName(),
                                    connection.getConsumer()
                                    .getServiceName()});
                    }
                }

                // Probably an internal module
                if (nbResolution == 0) {
                    LOG.log(Level.FINE, "Unresolved consumes : {}",
                            connection.getConsumer().getEndpointName());

                    Connector connector = new Connector();
                    connector.setType("sun-bpel-engine");
                    connector.setName(connection.getConsumer()
                            .getInterfaceName().getLocalPart());
                    connector.setConnectionPoint(ConnectionPointBuilder
                            .getConnectionPoint(connection.getConsumer()));
                    connectorsMap.put(
                            UIDUtils.getUID(connection.getConsumer()),
                            connector);
                }

                model.getLinks().add(link);
            }

            model.getConnectors().addAll(connectorsMap.values());

        } catch (Exception jaxbe) {
            LOG.log(Level.SEVERE, "Unable to create map for service assembly", jaxbe);
        }

        return model;
    }
}
