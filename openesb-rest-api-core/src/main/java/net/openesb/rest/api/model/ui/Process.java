package net.openesb.rest.api.model.ui;

import java.util.List;

/**
 * 
 * @author Loic DASSONVILLE (ldassonville.openesb at gmail.com)
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class Process {

    private String name;
    private List<Endpoint> inputs;
    private List<Endpoint> outputs;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Endpoint> getInputs() {
        return inputs;
    }

    public void setInputs(List<Endpoint> inputs) {
        this.inputs = inputs;
    }

    public List<Endpoint> getOutputs() {
        return outputs;
    }

    public void setOutputs(List<Endpoint> outputs) {
        this.outputs = outputs;
    }
}
