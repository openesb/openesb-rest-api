package net.openesb.rest.api;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;
import net.openesb.rest.api.feature.ManagementFeature;
import net.openesb.rest.api.provider.ComponentNotFoundExceptionMapper;
import net.openesb.rest.api.provider.CorsResponseFilter;
import net.openesb.rest.api.provider.ManagementExceptionMapper;
import net.openesb.rest.api.provider.ObjectMapperProvider;
import net.openesb.rest.api.resources.ComponentsResource;
import net.openesb.rest.api.resources.InstanceResource;
import net.openesb.rest.api.resources.JVMInformationsResource;
import net.openesb.rest.api.resources.MessageServiceResource;
import net.openesb.rest.api.resources.ServiceAssembliesResource;
import net.openesb.rest.api.resources.SharedLibrariesResource;
import net.openesb.rest.api.resources.ui.CASAViewerResource;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public abstract class ManagementApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<Class<?>>();

        // Register Root Resources
        classes.add(InstanceResource.class);
        classes.add(ServiceAssembliesResource.class);
        classes.add(ComponentsResource.class);
        classes.add(SharedLibrariesResource.class);
        classes.add(MessageServiceResource.class);
        classes.add(JVMInformationsResource.class);
        classes.add(CASAViewerResource.class);

        // Register Exception mapper
        classes.add(ManagementExceptionMapper.class);
        classes.add(ComponentNotFoundExceptionMapper.class);

        // Register binder
        classes.add(ManagementFeature.class);

        // Register filter
        classes.add(CorsResponseFilter.class);

        // Register Jackson and external features
        classes.add(ObjectMapperProvider.class);
        classes.add(JacksonFeature.class);
        classes.add(MultiPartFeature.class);
        
        return classes;
    }
}