package net.openesb.rest.api.provider;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 * Allow the system to serve xhr level 2 from all cross domain site
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Provider
@PreMatching
public class CorsResponseFilter implements ContainerResponseFilter {

    private final static Logger log = Logger.getLogger(CorsResponseFilter.class.getName());

    /**
     * Add the cross domain data to the output if needed.
     *
     * @param reqCtx The container request (input)
     * @param respCtx The container request (output)
     * @throws IOException
     */
    @Override
    public void filter(ContainerRequestContext reqCtx, ContainerResponseContext respCtx) throws IOException {
        if (log.isLoggable(Level.FINEST)) {
            log.log(Level.FINEST, "Executing CORS response filter");
        }

        respCtx.getHeaders().add("Access-Control-Allow-Origin", "*");
        respCtx.getHeaders().add( "Access-Control-Allow-Headers", "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With");
        respCtx.getHeaders().add("Access-Control-Allow-Credentials", "true");
        respCtx.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS, X-XSRF-TOKEN");
        respCtx.getHeaders().add("Access-Control-Max-Age", "1209600");
    }
}
