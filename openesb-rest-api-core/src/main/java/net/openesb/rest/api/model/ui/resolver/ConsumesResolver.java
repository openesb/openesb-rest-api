package net.openesb.rest.api.model.ui.resolver;

import com.sun.jbi.management.descriptor.Connection;
import com.sun.jbi.management.descriptor.Consumer;
import com.sun.jbi.management.descriptor.Consumes;
import com.sun.jbi.management.descriptor.Jbi;
import com.sun.jbi.management.descriptor.Services;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Loic DASSONVILLE (ldassonville.openesb at gmail.com)
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ConsumesResolver extends Resolver<Consumes> {

    public ConsumesResolver(Jbi jbiServiceUnit, String targetComponent) {
        super(jbiServiceUnit, targetComponent);
    }

    @Override
    public ConnectionPoint toConnectionPoint(Connection connection) {
        Consumer consumer = connection.getConsumer();
        return new ConnectionPoint(consumer.getInterfaceName(), consumer.getServiceName(), consumer.getEndpointName());
    }

    @Override
    protected ConnectionPoint toConnectionPoint(Consumes consumes) {
        return new ConnectionPoint(consumes.getInterfaceName(), consumes.getServiceName(), consumes.getEndpointName());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<Consumes> extractEntities(Services services) {
        if (services == null) {
            return new ArrayList<Consumes>();
        }
        return services.getConsumes();
    }
}
