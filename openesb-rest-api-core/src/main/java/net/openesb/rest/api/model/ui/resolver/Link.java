package net.openesb.rest.api.model.ui.resolver;

/**
 * 
 * @author Loic DASSONVILLE (ldassonville.openesb at gmail.com)
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class Link {

    private String type;
    private ConnectionPoint source;
    private ConnectionPoint target;

    public Link() {
    }

    public Link(ConnectionPoint source, ConnectionPoint target) {
        this.source = source;
        this.target = target;
    }

    public ConnectionPoint getSource() {
        return source;
    }

    public void setSource(ConnectionPoint source) {
        this.source = source;
    }

    public ConnectionPoint getTarget() {
        return target;
    }

    public void setTarget(ConnectionPoint target) {
        this.target = target;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Link other = (Link) obj;
        if (this.source != other.source && (this.source == null || !this.source.equals(other.source))) {
            return false;
        }
        if (this.target != other.target && (this.target == null || !this.target.equals(other.target))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + (this.source != null ? this.source.hashCode() : 0);
        hash = 17 * hash + (this.target != null ? this.target.hashCode() : 0);
        return hash;
    }
}