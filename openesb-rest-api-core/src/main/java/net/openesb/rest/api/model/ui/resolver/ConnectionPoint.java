package net.openesb.rest.api.model.ui.resolver;

import javax.xml.namespace.QName;

/**
 * 
 * @author ldassonville (ldassonville.openesb at gmail.com)
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ConnectionPoint {

    private String endpointName;
    
    private QName serviceName;
    private QName interfaceName;

    public ConnectionPoint(QName interfaceName, QName serviceName, String endpointName) {
        this(serviceName, endpointName);
        this.interfaceName = interfaceName;
    }

    public ConnectionPoint(QName serviceName, String endpointName) {
        this.endpointName = endpointName;
        this.serviceName = serviceName;
    }

    public String getEndpointName() {
        return endpointName;
    }

    public QName getServiceName() {
        return serviceName;
    }

    public QName getInterfaceName() {
        return interfaceName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ConnectionPoint other = (ConnectionPoint) obj;
        if ((this.endpointName == null) ? (other.endpointName != null) : !this.endpointName.equals(other.endpointName)) {
            return false;
        }
        if (this.serviceName != other.serviceName && (this.serviceName == null || !this.serviceName.equals(other.serviceName))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + (this.endpointName != null ? this.endpointName.hashCode() : 0);
        hash = 23 * hash + (this.serviceName != null ? this.serviceName.hashCode() : 0);
        return hash;
    }
}