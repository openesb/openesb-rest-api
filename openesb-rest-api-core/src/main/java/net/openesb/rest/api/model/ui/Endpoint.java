package net.openesb.rest.api.model.ui;

import net.openesb.rest.api.model.ui.resolver.ConnectionPoint;

/**
 * 
 * @author Loic DASSONVILLE (ldassonville.openesb at gmail.com)
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class Endpoint {

    private String name;
    private ConnectionPoint connectionPoint;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ConnectionPoint getConnectionPoint() {
        return connectionPoint;
    }

    public void setConnectionPoint(ConnectionPoint connectionPoint) {
        this.connectionPoint = connectionPoint;
    }
}
