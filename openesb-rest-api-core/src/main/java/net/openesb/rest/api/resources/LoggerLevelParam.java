package net.openesb.rest.api.resources;

import java.util.logging.Level;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class LoggerLevelParam {
    
    private final Level level;
    
    public LoggerLevelParam(String input) {
        if (input != null && input.equalsIgnoreCase("default")) {
            level = null;
        }
        else {
            level = Level.parse(input.toUpperCase());
        }
    }
    
    public Level getLevel() {
        return this.level;
    }
}
