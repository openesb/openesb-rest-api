package net.openesb.rest.api.feature;

import javax.ws.rs.core.Feature;
import javax.ws.rs.core.FeatureContext;
import net.openesb.rest.api.inject.ManagementServiceBinder;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ManagementFeature implements Feature {

    @Override
    public boolean configure(final FeatureContext context) {
        context.register(new ManagementServiceBinder());
        
        return true;
    }
}
