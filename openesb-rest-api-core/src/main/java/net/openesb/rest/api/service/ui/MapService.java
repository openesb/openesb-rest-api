package net.openesb.rest.api.service.ui;

import net.openesb.management.api.ManagementException;
import net.openesb.rest.api.model.ui.Model;

/**
 *
 * @author Loic DASSONVILLE (ldassonville.openesb at gmail.com)
 * @author OpenESB Community
 */
public interface MapService {
        
	/**
	 * Return the meta-model of service assembly
	 * 
	 * @param assemblyName
	 * @return
	 * @throws ManagementException
	 */
    Model getMapModel(String assemblyName) throws ManagementException;
}
