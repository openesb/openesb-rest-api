package net.openesb.rest.api.resources.ui;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import net.openesb.management.api.ManagementException;
import net.openesb.rest.api.annotation.RequiresAuthentication;
import net.openesb.rest.api.model.ui.Model;
import net.openesb.rest.api.resources.AbstractResource;
import net.openesb.rest.api.service.ui.MapService;

/**
 *
 * @author Loic DASSONVILLE (ldassonville at gmail.com)
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Path("ui")
@RequiresAuthentication
public class CASAViewerResource extends AbstractResource {
    
    @Context
    private MapService mapService;
    
    /**
     * Provide assembly JSON model 
     * @param assemblyName 
     * @return JSON model
     * @throws ManagementException
     */
    @GET
    @Path("map/{assemblyName}")
    @Produces(MediaType.APPLICATION_JSON)
    public Model getMapModel(@PathParam("assemblyName") String assemblyName) throws ManagementException {
    	return mapService.getMapModel(assemblyName);
    }
}
