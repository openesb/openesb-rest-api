package net.openesb.rest.api.provider;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import net.openesb.management.api.ManagementException;
import net.openesb.model.api.manage.Task;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Provider
public class ManagementExceptionMapper implements ExceptionMapper<ManagementException> {

    @Override
    public Response toResponse(ManagementException me) {
        String message = me.getMessage();
        
        if (me.getTask() != null) {
            message = buildTaskError(me.getTask());
        }
        
        return Response.serverError().entity(message).build();
    }
    
    private String buildTaskError(Task task) {
        if (task.getFrameworkTask() != null) {
            if (! task.getFrameworkTask().getResult().getTaskStatusMsg().isEmpty()) {
                return task.getFrameworkTask().getResult().getTaskStatusMsg().get(0);
            } else {
                return task.getFrameworkTask().getResult().getErrorMsg().get(0);
            }
        } else {
            return task.getComponentsTask().iterator().next().getResult().getTaskStatusMsg().get(0);
        }
    }
}
