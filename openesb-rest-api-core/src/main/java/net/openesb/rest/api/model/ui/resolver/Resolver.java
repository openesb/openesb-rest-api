package net.openesb.rest.api.model.ui.resolver;

import com.sun.jbi.management.descriptor.Connection;
import com.sun.jbi.management.descriptor.Jbi;
import com.sun.jbi.management.descriptor.Services;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Loic DASSONVILLE (ldassonville.openesb at gmail.com)
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public abstract class Resolver<V> {

    private Map<ConnectionPoint, V> mapLink;
    /**
     * Component toResolve *
     */
    private String targetComponent;
    /**
     * JBI file *
     */
    private Jbi jbiServiceUnit;

    public Resolver(Jbi jbiServiceUnit, String targetComponent) {
        this.jbiServiceUnit = jbiServiceUnit;
        this.targetComponent = targetComponent;
        this.mapLink = map(jbiServiceUnit);
    }

    /**
     * Resolve connection
     *
     * @param serviceAssembly
     * @throws Exception
     */
    public V resolve(Connection connection) {
        ConnectionPoint providerConnectionLink = toConnectionPoint(connection);
        return mapLink.get(providerConnectionLink);
    }

    /**
     * Extract and Hash JBI
     *
     * @param jbi
     * @return
     */
    protected Map<ConnectionPoint, V> map(Jbi jbi) {
        Map<ConnectionPoint, V> res = new HashMap<ConnectionPoint, V>();
        Services services = jbiServiceUnit.getServices();
        if (services != null) {
            for (V consumes : extractEntities(services)) {
                res.put(toConnectionPoint(consumes), consumes);
            }
        }
        return res;
    }

    public Jbi getJbiServiceUnit() {
        return jbiServiceUnit;
    }

    public String getTargetComponent() {
        return targetComponent;
    }
    
    /**
     * Convert connection to connectionLink
     *
     * @param connection
     * @return
     */
    protected abstract ConnectionPoint toConnectionPoint(Connection connection);

    protected abstract ConnectionPoint toConnectionPoint(V entity);

    protected abstract List<V> extractEntities(Services services);
}
