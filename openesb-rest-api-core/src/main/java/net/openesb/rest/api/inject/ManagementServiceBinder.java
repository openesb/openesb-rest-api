package net.openesb.rest.api.inject;

import net.openesb.management.api.AdministrationService;
import net.openesb.management.api.ComponentService;
import net.openesb.management.api.ConfigurationService;
import net.openesb.management.api.EndpointService;
import net.openesb.management.api.JvmMetricsService;
import net.openesb.management.api.ManagementFactory;
import net.openesb.management.api.MessageService;
import net.openesb.management.api.ServiceAssemblyService;
import net.openesb.management.api.SharedLibraryService;
import net.openesb.management.api.StatisticsService;
import net.openesb.rest.api.service.ui.MapService;
import net.openesb.rest.api.service.ui.MapServiceImpl;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ManagementServiceBinder extends AbstractBinder {

    @Override
    protected void configure() {
        bind(ManagementFactory.getServiceAssemblyService()).to(ServiceAssemblyService.class);
        bind(ManagementFactory.getAdministrationService()).to(AdministrationService.class);
        bind(ManagementFactory.getComponentService()).to(ComponentService.class);
        bind(ManagementFactory.getConfigurationService()).to(ConfigurationService.class);
        bind(ManagementFactory.getEndpointService()).to(EndpointService.class);
        bind(ManagementFactory.getMessageService()).to(MessageService.class);
        bind(ManagementFactory.getSharedLibraryService()).to(SharedLibraryService.class);
        bind(ManagementFactory.getStatisticsService()).to(StatisticsService.class);
        bind(ManagementFactory.getJvmMetricsService()).to(JvmMetricsService.class);
        bind(new MapServiceImpl()).to(MapService.class);
    }
    
}