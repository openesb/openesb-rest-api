package net.openesb.rest.api.resources;

import java.io.File;
import java.io.InputStream;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import net.openesb.management.api.AdministrationService;
import net.openesb.rest.utils.FileUtils;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public abstract class AbstractResource {
    
    private static Logger sLogger = null;
    
    @Context
    private AdministrationService administrationService;
    
    /**
     * Get the logger for the REST services.
     *
     * @return java.util.Logger is the logger
     */
    protected Logger getLogger() {
        // late binding.
        if (sLogger == null) {
            sLogger = Logger.getLogger("net.openesb.rest.api");
        }
        return sLogger;
    }
    
    protected File createTemporaryFile(InputStream is, String fileName) {
        File uploadedFile = new File(
                administrationService.getTempStore(), fileName);
        FileUtils.writeToFile(is, uploadedFile);
        
        return uploadedFile;
    }
}
