package net.openesb.admin.rest.api;

import java.util.Set;
import javax.ws.rs.ApplicationPath;
import net.openesb.rest.api.ManagementApplication;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@ApplicationPath("api")
public class ContainerManagementApplication extends ManagementApplication {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = super.getClasses();

        // Register filter
        classes.add(ContainerBasedAuthenticationFilter.class);
        
        return classes;
    }
}
