package net.openesb.admin.rest.api;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Provider
@PreMatching
public class ContainerBasedAuthenticationFilter implements ContainerRequestFilter {

    private final static Logger log = Logger.getLogger(ContainerBasedAuthenticationFilter.class.getName());

    @Context
    HttpServletRequest request;

    @Override
    public void filter(ContainerRequestContext requestCtx) throws IOException {

        // When HttpMethod comes as OPTIONS, just acknowledge that it accepts...
        // In the real world, this should more sophisticated
        if (requestCtx.getRequest().getMethod().equals("OPTIONS")) {
            // Just send a OK signal back to the browser
            requestCtx.abortWith(Response.status(Response.Status.OK).build());
        } else {

            //try to authenticate
            try {

                String username = "";
                String password = "";

                String authorization = request.getHeader("authorization");

                if (null != authorization && authorization.length() > "Basic ".length()) {
                    String usernamePassword = new String(javax.xml.bind.DatatypeConverter.parseBase64Binary(authorization.substring("Basic ".length())));
                    if (usernamePassword.contains(":")) {
                        username = usernamePassword.substring(0, usernamePassword.indexOf(":"));
                        if (usernamePassword.indexOf(":") + 1 < usernamePassword.length()) {
                            password = usernamePassword.substring(usernamePassword.indexOf(":") + 1);
                        }
                    }
                }

                try {
                    request.login(username, password);
                } catch (ServletException ex) {
                    log.log(Level.SEVERE, "Unexpected error while login", ex);
                    requestCtx.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
                    return;
                }

                if (log.isLoggable(Level.FINEST)) {
                    log.log(Level.FINEST, "The authenticated user is in role: {0}", request.isUserInRole("oeadmin"));
                    log.log(Level.FINEST, "The authenticated remote username: {0}", request.getRemoteUser());
                    log.log(Level.FINEST, "The authenticated Principal name: {0}", request.getUserPrincipal());
                    log.log(Level.FINEST, "The authentication type: {0}", request.getAuthType());
                }

                String requestUriPath = requestCtx.getUriInfo().getRequestUri().getPath();

                if (requestUriPath.endsWith("/login")) {
                    requestCtx.abortWith(Response.status(Response.Status.OK).build());
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    request.logout();
                } catch (ServletException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}
